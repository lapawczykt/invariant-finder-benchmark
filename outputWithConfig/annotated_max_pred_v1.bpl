// Boogie program verifier version 2.3.0.61016, Copyright (c) 2003-2014, Microsoft.
// Command Line Options: /proc:max

function contains(v: int, a: [int]int, n: int) : bool;

axiom (forall v: int, a: [int]int, n: int :: { contains(v, a, n): bool } contains(v, a, n): bool <==> (exists j: int :: 0 <= j && j < n && a[j] == v));

function upper_bound(v: int, a: [int]int, n: int) : bool;

axiom (forall v: int, a: [int]int, n: int :: { upper_bound(v, a, n): bool } upper_bound(v, a, n): bool <==> (forall j: int :: 0 <= j && j < n ==> a[j] <= v));

procedure max(a: [int]int, n: int) returns (max: int);
  requires n > 0;
  ensures contains(max, a, n);
  ensures upper_bound(max, a, n);



implementation max(a: [int]int, n: int) returns (max: int)
{
  var i: int;

    max := a[0];
    i := 1;
    while (i < n)
      invariant n >= i;
      invariant max >= max;
      invariant contains(max, a, i);
      invariant upper_bound(max, a, i);
    {
        if (a[i] > max)
        {
            max := a[i];
        }

        i := i + 1;
    }

    return;
}


