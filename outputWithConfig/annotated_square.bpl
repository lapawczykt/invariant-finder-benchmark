// Boogie program verifier version 2.3.0.61016, Copyright (c) 2003-2014, Microsoft.
// Command Line Options: /proc:square

procedure square(n: int) returns (r: int);
  requires 0 <= n;
  ensures r == n * n;



implementation square(n: int) returns (r: int)
{
  var x: int;
  var i: int;

    r := 0;
    i := 0;
    x := 1;
    while (i < n)
      invariant i + 1 <= n + 1;
      invariant i + 1 <= r + 1;
      invariant 2 <= x + 1;
      invariant 2 <= 2;
      invariant x + 1 == 2 * (i + 1);
      invariant r == i * i;
    {
        r := r + x;
        x := x + 2;
        i := i + 1;
    }
}


