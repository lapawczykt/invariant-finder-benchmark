// Boogie program verifier version 2.3.0.61016, Copyright (c) 2003-2014, Microsoft.
// Command Line Options: /proc:add_small_numbers

procedure add_small_numbers(a: [int]int, n: int, max: int) returns (r: int);
  requires n > 0;
  requires (forall i: int :: 0 <= i && i < n ==> a[i] <= max);
  ensures r <= max * n;



implementation add_small_numbers(a: [int]int, n: int, max: int) returns (r: int)
{
  var i: int;

    i := 0;
    r := 0;
    while (i < n)
      invariant i <= n;
      invariant i <= i;
      invariant r <= max * i;
    {
        r := r + a[i];
        i := i + 1;
    }
}


