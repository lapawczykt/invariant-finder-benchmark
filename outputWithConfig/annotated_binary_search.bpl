// Boogie program verifier version 2.3.0.61016, Copyright (c) 2003-2014, Microsoft.
// Command Line Options: /proc:binary_search

function sorted(a: [int]int, fromIndex: int, toIndex: int) : bool;

axiom (forall a: [int]int, fromIndex: int, toIndex: int :: { sorted(a, fromIndex, toIndex): bool } sorted(a, fromIndex, toIndex): bool <==> (forall i: int, j: int :: fromIndex <= i && i <= j && j < toIndex ==> a[i] <= a[j]));

function contains(a: [int]int, fromIndex: int, toIndex: int, key: int) : bool;

axiom (forall a: [int]int, fromIndex: int, toIndex: int, key: int :: { contains(a, fromIndex, toIndex, key): bool } contains(a, fromIndex, toIndex, key): bool <==> (exists j: int :: fromIndex <= j && j < toIndex && a[j] == key));

procedure binary_search(a: [int]int, fromIndex: int, toIndex: int, key: int) returns (index: int);
  requires fromIndex <= toIndex;
  requires sorted(a, fromIndex, toIndex);
  ensures index < toIndex ==> a[index] == key;
  ensures index == toIndex ==> !contains(a, fromIndex, toIndex, key);



implementation binary_search(a: [int]int, fromIndex: int, toIndex: int, key: int) returns (index: int)
{
  var low: int;
  var high: int;

    low, high := fromIndex, toIndex - 1;
    while (low <= high)
      invariant toIndex < high + 1 ==> a[toIndex] == high;
      invariant toIndex < high + 1 ==> a[toIndex] == high + 1;
      invariant low + 1 < fromIndex + 1 ==> a[low + 1] == high + 1;
      invariant high + 1 < low ==> a[high + 1] == high;
      invariant high + 1 < low ==> a[high + 1] == high + 1;
      invariant fromIndex == high + 1 ==> !contains(a, fromIndex, high + 1, fromIndex);
      invariant fromIndex + 1 == fromIndex + 1 ==> !contains(a, low + 1, fromIndex + 1, high + 1);
      invariant fromIndex + 1 == toIndex ==> !contains(a, fromIndex + 1, toIndex, fromIndex);
      invariant toIndex == toIndex ==> !contains(a, high + 1, toIndex, key);
      invariant toIndex == toIndex + 1 ==> !contains(a, fromIndex, toIndex + 1, fromIndex);
      invariant key == toIndex ==> !contains(a, high + 1, toIndex, fromIndex);
      invariant key == toIndex ==> !contains(a, high + 1, toIndex, fromIndex + 1);
      invariant key == toIndex ==> !contains(a, high + 1, toIndex, high);
      invariant key == toIndex ==> !contains(a, high + 1, toIndex, high + 1);
      invariant key + 1 == toIndex ==> !contains(a, high + 1, toIndex, fromIndex);
      invariant key + 1 == toIndex ==> !contains(a, high + 1, toIndex, key);
      invariant key + 1 == toIndex ==> !contains(a, high + 1, toIndex, high);
      invariant key + 1 == toIndex ==> !contains(a, high + 1, toIndex, high + 1);
      invariant index == toIndex ==> !contains(a, high + 1, toIndex, fromIndex);
      invariant index == toIndex ==> !contains(a, high + 1, toIndex, fromIndex + 1);
      invariant index == toIndex ==> !contains(a, high + 1, toIndex, key + 1);
      invariant index == toIndex ==> !contains(a, high + 1, toIndex, index);
      invariant index == toIndex ==> !contains(a, high + 1, toIndex, index + 1);
      invariant index == toIndex ==> !contains(a, high + 1, toIndex, low);
      invariant index == toIndex ==> !contains(a, high + 1, toIndex, low + 1);
      invariant index == toIndex ==> !contains(a, high + 1, toIndex, high);
      invariant index == toIndex ==> !contains(a, high + 1, toIndex, high + 1);
      invariant index == toIndex + 1 ==> !contains(a, toIndex + 1, toIndex + 1, fromIndex);
      invariant low == fromIndex + 1 ==> !contains(a, index, fromIndex + 1, key);
      invariant low == fromIndex + 1 ==> !contains(a, index, fromIndex + 1, key + 1);
      invariant low == fromIndex + 1 ==> !contains(a, index + 1, fromIndex + 1, fromIndex);
      invariant low == fromIndex + 1 ==> !contains(a, index + 1, fromIndex + 1, fromIndex + 1);
      invariant low == fromIndex + 1 ==> !contains(a, index + 1, fromIndex + 1, toIndex);
      invariant low == fromIndex + 1 ==> !contains(a, index + 1, fromIndex + 1, toIndex + 1);
      invariant low == fromIndex + 1 ==> !contains(a, index + 1, fromIndex + 1, index);
      invariant low == fromIndex + 1 ==> !contains(a, index + 1, fromIndex + 1, index + 1);
      invariant low == fromIndex + 1 ==> !contains(a, index + 1, fromIndex + 1, low + 1);
      invariant low == fromIndex + 1 ==> !contains(a, index + 1, fromIndex + 1, high);
      invariant low == fromIndex + 1 ==> !contains(a, index + 1, fromIndex + 1, high + 1);
      invariant low == fromIndex + 1 ==> !contains(a, low, fromIndex + 1, fromIndex);
      invariant low == key ==> !contains(a, fromIndex, key, toIndex);
      invariant low == key ==> !contains(a, fromIndex, key, toIndex + 1);
      invariant low == key + 1 ==> !contains(a, fromIndex, key + 1, toIndex);
      invariant low == key + 1 ==> !contains(a, fromIndex, key + 1, toIndex + 1);
      invariant low == key + 1 ==> !contains(a, fromIndex, key + 1, low + 1);
      invariant low == key + 1 ==> !contains(a, fromIndex + 1, key + 1, toIndex);
      invariant low == low ==> !contains(a, fromIndex, low, key);
      invariant low == low ==> !contains(a, fromIndex, low, key + 1);
    {
        index := low + (high - low) div 2;
        if (a[index] < key)
        {
            low := index + 1;
        }
        else
        {
            if (a[index] > key)
            {
                high := index - 1;
            }
            else
            {
                return;
            }
        }
    }

    index := toIndex;
}


