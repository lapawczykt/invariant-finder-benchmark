// Boogie program verifier version 2.3.0.61016, Copyright (c) 2003-2014, Microsoft.
// Command Line Options: /proc:even_true

function has_even(v: bool, a: [int]bool, n: int) : bool;

axiom (forall v: bool, a: [int]bool, n: int :: n <= 0 ==> has_even(v, a, n));

axiom (forall v: bool, a: [int]bool, n: int :: (a[n] <==> v) ==> (has_even(v, a, n) <==> !has_even(v, a, n + 1)));

axiom (forall v: bool, a: [int]bool, n: int :: (a[n] <==> !v) ==> (has_even(v, a, n) <==> has_even(v, a, n + 1)));

procedure even_true(a: [int]bool, n: int) returns (even: bool);
  requires n >= 0;
  ensures has_even(true, a, n) <==> even;



implementation even_true(a: [int]bool, n: int) returns (even: bool)
{
  var i: int;

    i := 0;
    even := true;
    while (i < n)
      invariant i <= n;
      invariant i <= i;
      invariant has_even(true, a, i) <==> even;
    {
        if (a[i])
        {
            even := !even;
        }

        i := i + 1;
    }
}


