// Boogie program verifier version 2.3.0.61016, Copyright (c) 2003-2014, Microsoft.
// Command Line Options: /proc:merge

function sorted(a: [int]int, fromIndex: int, toIndex: int) : bool;

axiom (forall a: [int]int, fromIndex: int, toIndex: int :: { sorted(a, fromIndex, toIndex): bool } sorted(a, fromIndex, toIndex): bool <==> (forall i: int, j: int :: fromIndex <= i && i <= j && j < toIndex ==> a[i] <= a[j]));

procedure merge(a: [int]int, start: int, mid: int, n: int) returns (b: [int]int);
  requires 0 <= start;
  requires start <= mid;
  requires mid <= n;
  requires sorted(a, start, mid);
  requires sorted(a, mid, n);
  ensures sorted(b, start, n);



implementation merge(a: [int]int, start: int, mid: int, n: int) returns (b: [int]int)
{
  var i: int;
  var l: int;
  var r: int;

    i, l, r := start, start, mid;
    while (i < n)
      invariant n >= i;
      invariant n >= l;
      invariant n >= r;
      invariant i >= start;
      invariant i >= l;
      invariant l >= start;
      invariant r >= mid;
      invariant r >= l;
      invariant (forall boundVar0: int, boundVar1: int :: start <= start && start < r && r < i ==> a[start] <= a[r]);
      invariant (forall boundVar0: int, boundVar1: int :: start <= start && start < r && r < l ==> a[start] <= a[r]);
      invariant (forall boundVar0: int, boundVar1: int :: i <= start && start < mid && mid < r ==> b[start] <= b[mid]);
      invariant (forall boundVar0: int, boundVar1: int :: i <= start && start < n && n < start ==> a[start] <= a[n]);
      invariant (forall boundVar0: int, boundVar1: int :: i <= start && start < boundVar1 && boundVar1 < r ==> a[start] <= a[boundVar1]);
      invariant (forall boundVar0: int, boundVar1: int :: i <= start && start < boundVar1 && boundVar1 < boundVar1 ==> a[start] <= a[boundVar1]);
      invariant (forall boundVar0: int, boundVar1: int :: r <= start && start < i && i < n ==> b[start] <= b[i]);
      invariant (forall boundVar0: int, boundVar1: int :: r <= start && start < i && i < i ==> a[start] <= a[i]);
      invariant (forall boundVar0: int, boundVar1: int :: r <= start && start < i && i < boundVar0 ==> a[start] <= a[i]);
      invariant (forall boundVar0: int, boundVar1: int :: r <= start && start < i && i < boundVar0 ==> b[start] <= b[i]);
      invariant (forall boundVar0: int, boundVar1: int :: r <= start && start < i && i < boundVar1 ==> a[start] <= a[i]);
      invariant (forall boundVar0: int, boundVar1: int :: r <= start && start < i && i < boundVar1 ==> b[start] <= b[i]);
      invariant (forall boundVar0: int, boundVar1: int :: r <= start && start < boundVar1 && boundVar1 < i ==> b[start] <= b[boundVar1]);
      invariant (forall boundVar0: int, boundVar1: int :: r <= start && start < boundVar1 && boundVar1 < l ==> a[start] <= a[boundVar1]);
    {
        if (r >= n || (l < mid && a[l] < a[r]))
        {
            b[i] := a[l];
            l := l + 1;
        }
        else
        {
            b[i] := a[r];
            r := r + 1;
        }

        i := i + 1;
    }
}


