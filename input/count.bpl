function has_count (v: int, a: [int]int, n: int) returns (int);
axiom (forall v: int, a: [int]int, n: int :: n <= 0 ==> has_count (v, a, n) == 0);
axiom (forall v: int, a: [int]int, n: int :: a[n] == v ==> has_count (v, a, n) + 1 == has_count (v, a, n+1));
axiom (forall v: int, a: [int]int, n: int :: a[n] != v ==> has_count (v, a, n) == has_count (v, a, n+1));

procedure count (v: int, a: [int]int, n: int) returns (r: int)
	requires n >= 0;
	ensures has_count (v, a, n) == r;
{
	var i: int;

	i := 0;
	r := 0;

	while (i < n)
		//invariant i <= n;
		//invariant has_count (v, a, i) == r;
	{
		if (a[i] == v)
		{
			r := r + 1;
		}
		i := i + 1;
	}
}
