function sorted(a: [int]int, fromIndex: int, toIndex: int) : bool;

axiom (forall a: [int]int, fromIndex: int, toIndex: int :: { sorted(a, fromIndex, toIndex): bool } sorted(a, fromIndex, toIndex): bool <==> (forall i: int, j: int :: fromIndex <= i && i <= j && j < toIndex ==> a[i] <= a[j]));

procedure merge(a: [int]int, start: int, mid: int, n: int) returns (b: [int]int)
	requires 0 <= start;
  requires start <= mid;
  requires mid <= n;
  requires sorted(a, start, mid);
  requires sorted(a, mid, n);
  ensures sorted(b, start, n);
{
  var i: int;
  var l: int;
  var r: int;

    i, l, r := start, start, mid;
    while (i < n)
			//invariant (forall j, k: int :: l <= j && j < k && k < mid ==> a[j] <= a[k]);
			//invariant (forall j, k: int :: r <= j && j < k && k < n ==> a[j] <= a[k]);
			//invariant l < mid ==> (forall j: int :: start <= j && j < i ==> b[j] <= a[l]);
			//invariant r < n ==> (forall j: int :: start <= j && j < i ==> b[j] <= a[r]);
			//invariant n - i == (mid - l) + (n - r);
			//invariant sorted (b, start, i);
    {
				if (r >= n || (l < mid && a[l] < a[r])) {
					b[i] := a[l];
					l := l + 1;
				} else {
					b[i] := a[r];
					r := r + 1;
				}
				i := i + 1;
    }
}


