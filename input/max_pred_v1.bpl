function contains (v: int, a: [int]int, n: int) returns (bool)
{ (exists j: int :: 0 <= j && j < n && a[j] == v) }

function upper_bound (v: int, a: [int]int, n: int) returns (bool)
{ (forall j: int :: 0 <= j && j < n ==> a[j] <= v) }

procedure max(a: [int]int, n : int) returns (max : int)
	requires n > 0;
	ensures contains (max, a, n);
	ensures upper_bound (max, a, n);
{
	var i : int;

	max := a[0];
	i := 1;

	while (i < n)
		//invariant i <= n;
		//invariant contains (max, a, i);
		//invariant upper_bound (max, a, i);
	{
		if (a[i] > max) {
			max := a[i];
		}
		i := i + 1;
	}
	return;
}
