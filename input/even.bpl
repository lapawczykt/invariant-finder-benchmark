function has_even (v: bool, a: [int]bool, n: int) returns (bool);
axiom (forall v: bool, a: [int]bool, n: int :: n <= 0 ==> has_even (v, a, n));
axiom (forall v: bool, a: [int]bool, n: int :: a[n] == v ==> has_even (v, a, n) != has_even (v, a, n+1));
axiom (forall v: bool, a: [int]bool, n: int :: a[n] != v ==> has_even (v, a, n) == has_even (v, a, n+1));

procedure even_true (a: [int]bool, n: int) returns (even: bool)
	requires n >= 0;
	ensures has_even (true, a, n) <==> even;
{
	var i: int;

	i := 0;
	even := true;

	while (i < n)
		//invariant i <= n;
		//invariant has_even (true, a, i) <==> even;
	{
		if (a[i])
		{
			even := !even;
		}
		i := i + 1;
	}
}
