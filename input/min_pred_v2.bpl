function contains (v: int, a: [int]int, n: int) returns (bool)
{ (exists j: int :: 0 <= j && j < n && a[j] == v) } 

function lower_bound (v: int, a: [int]int, n: int) returns (bool)
{ (forall j: int :: 0 <= j && j < n ==> a[j] >= v) }

function is_min (m: int, a: [int]int, n: int) returns (bool)
{ contains (m, a, n) && lower_bound (m, a, n) }

procedure min(a: [int]int, n : int) returns (min : int)
	requires n > 0;
	ensures is_min (min, a, n);
{
	var i : int;

	min := a[0];
	i := 1;

	while (i < n)
		//invariant i <= n;
		//invariant is_min (min, a, i);
	{
		if (a[i] < min) {
			min := a[i];
		}
		i := i + 1;
	}
	return;
}
