function sorted (a: [int]int, fromIndex: int, toIndex: int) returns (bool)
{ (forall i: int, j: int :: fromIndex <= i && i <= j && j < toIndex ==> a[i] <= a[j]) }

function contains (a: [int]int, fromIndex: int, toIndex: int, key: int) returns (bool)
{ (exists j: int :: fromIndex <= j && j < toIndex && a[j] == key) } 

procedure binary_search (a: [int]int, fromIndex: int, toIndex: int, key: int) returns (index: int)
	requires fromIndex <= toIndex;
	requires sorted(a, fromIndex, toIndex);
	ensures index < toIndex ==> a[index] == key;
	ensures index == toIndex ==> (!contains(a, fromIndex, toIndex, key));
{
	var low, high: int;
	low, high := fromIndex, toIndex - 1;

	while (low <= high)
		//invariant high < toIndex;
		//invariant low >= fromIndex;
		//invariant !contains (a, fromIndex, low, key);
		//invariant !contains (a, high + 1, toIndex, key);
	{
		index := low + ( (high - low) div 2);
		if (a[index] < key) {
			low := index + 1;
		}
		else {
			if (a[index] > key) {
				high := index - 1;
			} else {
				return;
			}
		}
	}
	index := toIndex;
}