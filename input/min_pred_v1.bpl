function contains (v: int, a: [int]int, n: int) returns (bool)
{ (exists j: int :: 0 <= j && j < n && a[j] == v) } 

function lower_bound (v: int, a: [int]int, n: int) returns (bool)
{ (forall j: int :: 0 <= j && j < n ==> a[j] >= v) }

procedure min(a: [int]int, n : int) returns (min : int)
	requires n > 0;
	ensures contains (min, a, n);
	ensures lower_bound (min, a, n);
{
	var i : int;

	min := a[0];
	i := 1;

	while (i < n)
		//invariant i <= n;
		//invariant contains (min, a, i);
		//invariant lower_bound (min, a, i);
	{
		if (a[i] < min) {
			min := a[i];
		}
		i := i + 1;
	}
	return;
}
