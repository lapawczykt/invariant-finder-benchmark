function even(n: int) : bool;
axiom even(0);
axiom (forall i: int :: i > 0 ==> (even(i) <==> !even(i - 1)));

procedure is_even (n: int) returns (r: bool)
	requires n >= 0;
	ensures r <==> even (n);
{
	var i: int;

	i := 0;
	r := true;

	while (i < n)
		//invariant i <= n;
		//invariant r <==> even (i);
	{
		r := !r;
		i := i + 1;
	}
}
