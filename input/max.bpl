procedure max(a: [int]int, n : int) returns (max : int)
	requires n > 0;
	ensures (exists i : int :: 0 <= i && i < n && a[i] == max);
	ensures (forall i : int :: 0 <= i && i < n ==> a[i] <= max);
{
	var i : int;

	max := a[0];
	i := 1;

	while (i < n)
		//invariant i <= n;
		//invariant (exists j : int :: 0 <= j && j < i && a[j] == max);
		//invariant (forall j : int :: 0 <= j && j < i ==> a[j] <= max);
	{
		if (a[i] > max) {
			max := a[i];
		}
		i := i + 1;
	}
	return;
}
