procedure max_orig(a: [int]int, n : int) returns (max : int)
	requires n > 0;
	ensures (exists i : int :: 0 <= i && i < n && a[i] == max);
	ensures (forall i : int :: 0 <= i && i < n ==> a[i] <= max);
{
	var i : int;

	max := a[0];
	i := 1;

	while (i < n)
		//invariant i <= n;
		//invariant (exists j : int :: 0 <= j && j < i && a[j] == max);
		//invariant (forall j : int :: 0 <= j && j < i ==> a[j] <= max);
	{
		if (a[i] > max) {
			max := a[i];
		}
		i := i + 1;
	}
	return;
}

function contains (v: int, a: [int]int, n: int) returns (bool)
{ (exists j: int :: 0 <= j && j < n && a[j] == v) }

function upper_bound (v: int, a: [int]int, n: int) returns (bool)
{ (forall j: int :: 0 <= j && j < n ==> a[j] <= v) }

procedure max_pred_v1(a: [int]int, n : int) returns (max : int)
	requires n > 0;
	ensures contains (max, a, n);
	ensures upper_bound (max, a, n);
{
	var i : int;

	max := a[0];
	i := 1;

	while (i < n)
		//invariant i <= n;
		//invariant contains (max, a, i);
		//invariant upper_bound (max, a, i);
	{
		if (a[i] > max) {
			max := a[i];
		}
		i := i + 1;
	}
	return;
}

function is_max (m: int, a: [int]int, n: int) returns (bool)
{ contains (m, a, n) && upper_bound (m, a, n) }

procedure max_pred_v2(a: [int]int, n : int) returns (max : int)
	requires n > 0;
	ensures is_max (max, a, n);
{
	var i : int;

	max := a[0];
	i := 1;

	while (i < n)
		//invariant i <= n;
		//invariant is_max (max, a, i);
	{
		if (a[i] > max) {
			max := a[i];
		}
		i := i + 1;
	}
	return;
}
