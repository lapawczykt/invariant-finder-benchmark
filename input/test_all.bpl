function sorted (a: [int]int, fromIndex: int, toIndex: int) returns (bool)
{ (forall i: int, j: int :: fromIndex <= i && i <= j && j < toIndex ==> a[i] <= a[j]) }

function contains_from_to (a: [int]int, fromIndex: int, toIndex: int, key: int) returns (bool)
{ (exists j: int :: fromIndex <= j && j < toIndex && a[j] == key) } 

procedure binary_search (a: [int]int, fromIndex: int, toIndex: int, key: int) returns (index: int)
	requires fromIndex <= toIndex;
	requires sorted(a, fromIndex, toIndex);
	ensures index < toIndex ==> a[index] == key;
	ensures index == toIndex ==> (!contains_from_to(a, fromIndex, toIndex, key));
{
	var low, high: int;
	low, high := fromIndex, toIndex - 1;

	while (low <= high)
		//invariant high < toIndex;
		//invariant low >= fromIndex;
		//invariant !contains_from_to (a, fromIndex, low, key);
		//invariant !contains_from_to (a, high + 1, toIndex, key);
	{
		index := low + ( (high - low) div 2);
		if (a[index] < key) {
			low := index + 1;
		}
		else {
			if (a[index] > key) {
				high := index - 1;
			} else {
				return;
			}
		}
	}
	index := toIndex;
}

function has_count (v: int, a: [int]int, n: int) returns (int);
axiom (forall v: int, a: [int]int, n: int :: n <= 0 ==> has_count (v, a, n) == 0);
axiom (forall v: int, a: [int]int, n: int :: a[n] == v ==> has_count (v, a, n) + 1 == has_count (v, a, n+1));
axiom (forall v: int, a: [int]int, n: int :: a[n] != v ==> has_count (v, a, n) == has_count (v, a, n+1));

procedure count (v: int, a: [int]int, n: int) returns (r: int)
	requires n >= 0;
	ensures has_count (v, a, n) == r;
{
	var i: int;

	i := 0;
	r := 0;

	while (i < n)
		//invariant i <= n;
		//invariant has_count (v, a, i) == r;
	{
		if (a[i] == v)
		{
			r := r + 1;
		}
		i := i + 1;
	}
}

function has_even (v: bool, a: [int]bool, n: int) returns (bool);
axiom (forall v: bool, a: [int]bool, n: int :: n <= 0 ==> has_even (v, a, n));
axiom (forall v: bool, a: [int]bool, n: int :: a[n] == v ==> has_even (v, a, n) != has_even (v, a, n+1));
axiom (forall v: bool, a: [int]bool, n: int :: a[n] != v ==> has_even (v, a, n) == has_even (v, a, n+1));

procedure even_true (a: [int]bool, n: int) returns (even: bool)
	requires n >= 0;
	ensures has_even (true, a, n) <==> even;
{
	var i: int;

	i := 0;
	even := true;

	while (i < n)
		//invariant i <= n;
		//invariant has_even (true, a, i) <==> even;
	{
		if (a[i])
		{
			even := !even;
		}
		i := i + 1;
	}
}

function even(n: int) : bool;
axiom even(0);
axiom (forall i: int :: i > 0 ==> (even(i) <==> !even(i - 1)));

procedure is_even (n: int) returns (r: bool)
	requires n >= 0;
	ensures r <==> even (n);
{
	var i: int;

	i := 0;
	r := true;

	while (i < n)
		//invariant i <= n;
		//invariant r <==> even (i);
	{
		r := !r;
		i := i + 1;
	}
}

procedure max_orig(a: [int]int, n : int) returns (max : int)
	requires n > 0;
	ensures (exists i : int :: 0 <= i && i < n && a[i] == max);
	ensures (forall i : int :: 0 <= i && i < n ==> a[i] <= max);
{
	var i : int;

	max := a[0];
	i := 1;

	while (i < n)
		//invariant i <= n;
		//invariant (exists j : int :: 0 <= j && j < i && a[j] == max);
		//invariant (forall j : int :: 0 <= j && j < i ==> a[j] <= max);
	{
		if (a[i] > max) {
			max := a[i];
		}
		i := i + 1;
	}
	return;
}

function contains (v: int, a: [int]int, n: int) returns (bool)
{ (exists j: int :: 0 <= j && j < n && a[j] == v) }

function upper_bound (v: int, a: [int]int, n: int) returns (bool)
{ (forall j: int :: 0 <= j && j < n ==> a[j] <= v) }

procedure max_pred_v1(a: [int]int, n : int) returns (max : int)
	requires n > 0;
	ensures contains (max, a, n);
	ensures upper_bound (max, a, n);
{
	var i : int;

	max := a[0];
	i := 1;

	while (i < n)
		//invariant i <= n;
		//invariant contains (max, a, i);
		//invariant upper_bound (max, a, i);
	{
		if (a[i] > max) {
			max := a[i];
		}
		i := i + 1;
	}
	return;
}

function is_max (m: int, a: [int]int, n: int) returns (bool)
{ contains (m, a, n) && upper_bound (m, a, n) }

procedure max_pred_v2(a: [int]int, n : int) returns (max : int)
	requires n > 0;
	ensures is_max (max, a, n);
{
	var i : int;

	max := a[0];
	i := 1;

	while (i < n)
		//invariant i <= n;
		//invariant is_max (max, a, i);
	{
		if (a[i] > max) {
			max := a[i];
		}
		i := i + 1;
	}
	return;
}

procedure merge(a: [int]int, start: int, mid: int, n: int) returns (b: [int]int)
	requires 0 <= start;
  requires start <= mid;
  requires mid <= n;
  requires sorted(a, start, mid);
  requires sorted(a, mid, n);
  ensures sorted(b, start, n);
{
  var i: int;
  var l: int;
  var r: int;

    i, l, r := start, start, mid;
    while (i < n)
			//invariant (forall j, k: int :: l <= j && j < k && k < mid ==> a[j] <= a[k]);
			//invariant (forall j, k: int :: r <= j && j < k && k < n ==> a[j] <= a[k]);
			//invariant l < mid ==> (forall j: int :: start <= j && j < i ==> b[j] <= a[l]);
			//invariant r < n ==> (forall j: int :: start <= j && j < i ==> b[j] <= a[r]);
			//invariant n - i == (mid - l) + (n - r);
			//invariant sorted (b, start, i);
    {
				if (r >= n || (l < mid && a[l] < a[r])) {
					b[i] := a[l];
					l := l + 1;
				} else {
					b[i] := a[r];
					r := r + 1;
				}
				i := i + 1;
    }
}

procedure min_orig(a: [int]int, n : int) returns (min : int)
	requires n > 0;
	ensures (exists i : int :: 0 <= i && i < n && a[i] == min);
	ensures (forall i : int :: 0 <= i && i < n ==> a[i] >= min);
{
	var i : int;

	min := a[0];
	i := 1;

	while (i < n)
		//invariant i <= n;
		//invariant (exists j : int :: 0 <= j && j < i && a[j] == min);
		//invariant (forall j : int :: 0 <= j && j < i ==> a[j] >= min);
	{
		if (a[i] < min) {
			min := a[i];
		}
		i := i + 1;
	}
	return;
}

procedure min_pred_v1(a: [int]int, n : int) returns (min : int)
	requires n > 0;
	ensures contains (min, a, n);
	ensures lower_bound (min, a, n);
{
	var i : int;

	min := a[0];
	i := 1;

	while (i < n)
		//invariant i <= n;
		//invariant contains (min, a, i);
		//invariant lower_bound (min, a, i);
	{
		if (a[i] < min) {
			min := a[i];
		}
		i := i + 1;
	}
	return;
}

function lower_bound (v: int, a: [int]int, n: int) returns (bool)
{ (forall j: int :: 0 <= j && j < n ==> a[j] >= v) }

function is_min (m: int, a: [int]int, n: int) returns (bool)
{ contains (m, a, n) && lower_bound (m, a, n) }

procedure min_pred_v2(a: [int]int, n : int) returns (min : int)
	requires n > 0;
	ensures is_min (min, a, n);
{
	var i : int;

	min := a[0];
	i := 1;

	while (i < n)
		//invariant i <= n;
		//invariant is_min (min, a, i);
	{
		if (a[i] < min) {
			min := a[i];
		}
		i := i + 1;
	}
	return;
}

procedure square_orig (n: int) returns (r: int)
	requires 0 <= n;
	ensures r == n*n;
{
	var x: int;
	var i: int;

	r := 0;
	i := 0;
	x := 1;

	while (i < n)
		//invariant i <= n;
		//invariant r == i*i;
		//invariant x == 2*i + 1;
	{
		r := r + x;
		x := x + 2;
		i := i + 1;
	}
}

procedure square_easy_v1 (n: int) returns (r: int)
	requires 0 <= n;
	ensures r == n*n;
{
	var i: int;

	i := 0;
	r := 0;

	while (i < n)
		//invariant i <= n;
		//invariant r == i*n;
	{
		r := r + n;
		i := i + 1;
	}
}

procedure square_easy_v2 (n: int, x: int) returns (r: int)
	requires 0 <= n;
	requires x == n;
	ensures r == x*n;
{
	var i: int;

	i := 0;
	r := 0;

	while (i < n)
		//invariant i <= n;
	  //invariant r == i * n;
	{
		r := r + n;
		i := i + 1;
	}
}
