// Boogie program verifier version 2.3.0.61016, Copyright (c) 2003-2014, Microsoft.
// Command Line Options: /proc:square

procedure square(n: int) returns (r: int);
  requires 0 <= n;
  ensures r == n * n;



implementation square(n: int) returns (r: int)
{
  var i: int;

    i := 0;
    r := 0;
    while (i < n)
      invariant n >= i;
      invariant n >= 0;
      invariant r >= i;
      invariant r >= 0;
      invariant i >= 0;
      invariant 0 >= 0;
    {
        r := r + n;
        i := i + 1;
    }
}


