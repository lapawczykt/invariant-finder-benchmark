// Boogie program verifier version 2.3.0.61016, Copyright (c) 2003-2014, Microsoft.
// Command Line Options: /proc:max

procedure max(a: [int]int, n: int) returns (max: int);
  requires n > 0;
  ensures (exists i: int :: 0 <= i && i < n && a[i] == max);
  ensures (forall i: int :: 0 <= i && i < n ==> a[i] <= max);



implementation max(a: [int]int, n: int) returns (max: int)
{
  var i: int;

    max := a[0];
    i := 1;
    while (i < n)
      invariant n >= i;
      invariant n >= 0;
      invariant (exists boundVar0: int :: 0 <= boundVar0 && boundVar0 < i && a[boundVar0] == max);
      invariant (forall boundVar0: int :: n <= n && n < n ==> a[n] <= n);
      invariant (forall boundVar0: int :: 0 <= boundVar0 && boundVar0 < i ==> a[boundVar0] <= max);
      invariant (forall boundVar0: int :: 0 <= boundVar0 && boundVar0 < boundVar0 ==> a[boundVar0] <= n);
    {
        if (a[i] > max)
        {
            max := a[i];
        }

        i := i + 1;
    }

    return;
}


