// Boogie program verifier version 2.3.0.61016, Copyright (c) 2003-2014, Microsoft.
// Command Line Options: /proc:binary_search

function sorted(a: [int]int, fromIndex: int, toIndex: int) : bool;

axiom (forall a: [int]int, fromIndex: int, toIndex: int :: { sorted(a, fromIndex, toIndex): bool } sorted(a, fromIndex, toIndex): bool <==> (forall i: int, j: int :: fromIndex <= i && i <= j && j < toIndex ==> a[i] <= a[j]));

function contains(a: [int]int, fromIndex: int, toIndex: int, key: int) : bool;

axiom (forall a: [int]int, fromIndex: int, toIndex: int, key: int :: { contains(a, fromIndex, toIndex, key): bool } contains(a, fromIndex, toIndex, key): bool <==> (exists j: int :: fromIndex <= j && j < toIndex && a[j] == key));

procedure binary_search(a: [int]int, fromIndex: int, toIndex: int, key: int) returns (index: int);
  requires fromIndex <= toIndex;
  requires sorted(a, fromIndex, toIndex);
  ensures index < toIndex ==> a[index] == key;
  ensures index == toIndex ==> !contains(a, fromIndex, toIndex, key);



implementation binary_search(a: [int]int, fromIndex: int, toIndex: int, key: int) returns (index: int)
{
  var low: int;
  var high: int;

    low, high := fromIndex, toIndex - 1;
    while (low <= high)
      invariant toIndex > high;
      invariant fromIndex <= fromIndex;
      invariant toIndex < low ==> a[toIndex] == high;
      invariant low < fromIndex ==> a[low] == 0;
      invariant fromIndex == fromIndex ==> !contains(a, low, fromIndex, 0);
      invariant fromIndex == toIndex ==> !contains(a, fromIndex, toIndex, fromIndex);
      invariant low == key ==> !contains(a, fromIndex, key, toIndex);
      invariant low == key ==> !contains(a, fromIndex, key, key);
      invariant low == low ==> !contains(a, fromIndex, low, key);
      invariant low == low ==> !contains(a, toIndex, low, fromIndex);
      invariant low == low ==> !contains(a, toIndex, low, high);
      invariant low == low ==> !contains(a, toIndex, low, 0);
    {
        index := low + (high - low) div 2;
        if (a[index] < key)
        {
            low := index + 1;
        }
        else
        {
            if (a[index] > key)
            {
                high := index - 1;
            }
            else
            {
                return;
            }
        }
    }

    index := toIndex;
}


