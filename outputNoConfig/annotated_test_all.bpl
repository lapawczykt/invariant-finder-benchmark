// Boogie program verifier version 2.3.0.61016, Copyright (c) 2003-2014, Microsoft.
// Command Line Options: /proc:square_easy_v2

function sorted(a: [int]int, fromIndex: int, toIndex: int) : bool;

axiom (forall a: [int]int, fromIndex: int, toIndex: int :: { sorted(a, fromIndex, toIndex): bool } sorted(a, fromIndex, toIndex): bool <==> (forall i: int, j: int :: fromIndex <= i && i <= j && j < toIndex ==> a[i] <= a[j]));

function contains_from_to(a: [int]int, fromIndex: int, toIndex: int, key: int) : bool;

axiom (forall a: [int]int, fromIndex: int, toIndex: int, key: int :: { contains_from_to(a, fromIndex, toIndex, key): bool } contains_from_to(a, fromIndex, toIndex, key): bool <==> (exists j: int :: fromIndex <= j && j < toIndex && a[j] == key));

procedure binary_search(a: [int]int, fromIndex: int, toIndex: int, key: int) returns (index: int);
  requires fromIndex <= toIndex;
  requires sorted(a, fromIndex, toIndex);
  ensures index < toIndex ==> a[index] == key;
  ensures index == toIndex ==> !contains_from_to(a, fromIndex, toIndex, key);



implementation binary_search(a: [int]int, fromIndex: int, toIndex: int, key: int) returns (index: int)
{
  var low: int;
  var high: int;

    low, high := fromIndex, toIndex - 1;
    while (low <= high)
      invariant toIndex > high;
      invariant fromIndex <= fromIndex;
      invariant toIndex < low ==> a[toIndex] == high;
      invariant low < fromIndex ==> a[low] == 0;
      invariant fromIndex == fromIndex ==> !contains_from_to(a, low, fromIndex, 0);
      invariant fromIndex == toIndex ==> !contains_from_to(a, fromIndex, toIndex, fromIndex);
      invariant low == key ==> !contains_from_to(a, fromIndex, key, toIndex);
      invariant low == key ==> !contains_from_to(a, fromIndex, key, key);
      invariant low == low ==> !contains_from_to(a, fromIndex, low, key);
      invariant low == low ==> !contains_from_to(a, toIndex, low, fromIndex);
      invariant low == low ==> !contains_from_to(a, toIndex, low, high);
      invariant low == low ==> !contains_from_to(a, toIndex, low, 0);
    {
        index := low + (high - low) div 2;
        if (a[index] < key)
        {
            low := index + 1;
        }
        else
        {
            if (a[index] > key)
            {
                high := index - 1;
            }
            else
            {
                return;
            }
        }
    }

    index := toIndex;
}



function has_count(v: int, a: [int]int, n: int) : int;

axiom (forall v: int, a: [int]int, n: int :: n <= 0 ==> has_count(v, a, n) == 0);

axiom (forall v: int, a: [int]int, n: int :: a[n] == v ==> has_count(v, a, n) + 1 == has_count(v, a, n + 1));

axiom (forall v: int, a: [int]int, n: int :: a[n] != v ==> has_count(v, a, n) == has_count(v, a, n + 1));

procedure count(v: int, a: [int]int, n: int) returns (r: int);
  requires n >= 0;
  ensures has_count(v, a, n) == r;



implementation count(v: int, a: [int]int, n: int) returns (r: int)
{
  var i: int;

    i := 0;
    r := 0;
    while (i < n)
      invariant n >= i;
      invariant n >= 0;
      invariant i >= 0;
      invariant 0 >= 0;
      invariant has_count(v, a, i) == r;
      invariant has_count(v, a, 0) == 0;
    {
        if (a[i] == v)
        {
            r := r + 1;
        }

        i := i + 1;
    }
}



function has_even(v: bool, a: [int]bool, n: int) : bool;

axiom (forall v: bool, a: [int]bool, n: int :: n <= 0 ==> has_even(v, a, n));

axiom (forall v: bool, a: [int]bool, n: int :: (a[n] <==> v) ==> (has_even(v, a, n) <==> !has_even(v, a, n + 1)));

axiom (forall v: bool, a: [int]bool, n: int :: (a[n] <==> !v) ==> (has_even(v, a, n) <==> has_even(v, a, n + 1)));

procedure even_true(a: [int]bool, n: int) returns (even: bool);
  requires n >= 0;
  ensures has_even(true, a, n) <==> even;



implementation even_true(a: [int]bool, n: int) returns (even: bool)
{
  var i: int;

    i := 0;
    even := true;
    while (i < n)
    {
        if (a[i])
        {
            even := !even;
        }

        i := i + 1;
    }
}



function even(n: int) : bool;

axiom even(0);

axiom (forall i: int :: i > 0 ==> (even(i) <==> !even(i - 1)));

procedure is_even(n: int) returns (r: bool);
  requires n >= 0;
  ensures r <==> even(n);



implementation is_even(n: int) returns (r: bool)
{
  var i: int;

    i := 0;
    r := true;
    while (i < n)
      invariant r <==> even(i);
    {
        r := !r;
        i := i + 1;
    }
}



procedure max_orig(a: [int]int, n: int) returns (max: int);
  requires n > 0;
  ensures (exists i: int :: 0 <= i && i < n && a[i] == max);
  ensures (forall i: int :: 0 <= i && i < n ==> a[i] <= max);



implementation max_orig(a: [int]int, n: int) returns (max: int)
{
  var i: int;

    max := a[0];
    i := 1;
    while (i < n)
      invariant 0 < i;
      invariant 0 <= n;
      invariant n >= i;
      invariant n >= 0;
      invariant (forall boundVar0: int :: 0 <= 0 && 0 < i ==> a[0] <= max);
      invariant (forall boundVar0: int :: 0 <= 0 && 0 < boundVar0 ==> a[0] <= max);
    {
        if (a[i] > max)
        {
            max := a[i];
        }

        i := i + 1;
    }

    return;
}



function contains(v: int, a: [int]int, n: int) : bool;

axiom (forall v: int, a: [int]int, n: int :: { contains(v, a, n): bool } contains(v, a, n): bool <==> (exists j: int :: 0 <= j && j < n && a[j] == v));

function upper_bound(v: int, a: [int]int, n: int) : bool;

axiom (forall v: int, a: [int]int, n: int :: { upper_bound(v, a, n): bool } upper_bound(v, a, n): bool <==> (forall j: int :: 0 <= j && j < n ==> a[j] <= v));

procedure max_pred_v1(a: [int]int, n: int) returns (max: int);
  requires n > 0;
  ensures contains(max, a, n);
  ensures upper_bound(max, a, n);



implementation max_pred_v1(a: [int]int, n: int) returns (max: int)
{
  var i: int;

    max := a[0];
    i := 1;
    while (i < n)
      invariant 0 < i;
      invariant n > 0;
      invariant n >= i;
      invariant i >= 0;
      invariant contains(max, a, n);
      invariant upper_bound(max, a, i);
    {
        if (a[i] > max)
        {
            max := a[i];
        }

        i := i + 1;
    }

    return;
}



function is_max(m: int, a: [int]int, n: int) : bool;

axiom (forall m: int, a: [int]int, n: int :: { is_max(m, a, n): bool } is_max(m, a, n): bool <==> contains(m, a, n) && upper_bound(m, a, n));

procedure max_pred_v2(a: [int]int, n: int) returns (max: int);
  requires n > 0;
  ensures is_max(max, a, n);



implementation max_pred_v2(a: [int]int, n: int) returns (max: int)
{
  var i: int;

    max := a[0];
    i := 1;
    while (i < n)
      invariant i > 0;
      invariant max <= max;
      invariant n >= i;
      invariant i >= i;
    {
        if (a[i] > max)
        {
            max := a[i];
        }

        i := i + 1;
    }

    return;
}



procedure merge(a: [int]int, start: int, mid: int, n: int) returns (b: [int]int);
  requires 0 <= start;
  requires start <= mid;
  requires mid <= n;
  requires sorted(a, start, mid);
  requires sorted(a, mid, n);
  ensures sorted(b, start, n);



implementation merge(a: [int]int, start: int, mid: int, n: int) returns (b: [int]int)
{
  var i: int;
  var l: int;
  var r: int;

    i, l, r := start, start, mid;
    while (i < n)
      invariant n >= i;
      invariant n >= l;
      invariant n >= r;
      invariant n >= 0;
      invariant i >= l;
      invariant i >= 0;
      invariant l >= start;
      invariant l >= l;
      invariant r >= mid;
      invariant r >= l;
    {
        if (r >= n || (l < mid && a[l] < a[r]))
        {
            b[i] := a[l];
            l := l + 1;
        }
        else
        {
            b[i] := a[r];
            r := r + 1;
        }

        i := i + 1;
    }
}



procedure min_orig(a: [int]int, n: int) returns (min: int);
  requires n > 0;
  ensures (exists i: int :: 0 <= i && i < n && a[i] == min);
  ensures (forall i: int :: 0 <= i && i < n ==> a[i] >= min);



implementation min_orig(a: [int]int, n: int) returns (min: int)
{
  var i: int;

    min := a[0];
    i := 1;
    while (i < n)
      invariant i > 0;
      invariant n <= n;
      invariant i <= n;
      invariant i <= i;
      invariant (forall boundVar0: int :: 0 <= boundVar0 && boundVar0 < i ==> a[boundVar0] >= min);
      invariant (forall boundVar0: int :: 0 <= boundVar0 && boundVar0 < boundVar0 ==> a[boundVar0] >= n);
    {
        if (a[i] < min)
        {
            min := a[i];
        }

        i := i + 1;
    }

    return;
}



procedure min_pred_v1(a: [int]int, n: int) returns (min: int);
  requires n > 0;
  ensures contains(min, a, n);
  ensures lower_bound(min, a, n);



implementation min_pred_v1(a: [int]int, n: int) returns (min: int)
{
  var i: int;

    min := a[0];
    i := 1;
    while (i < n)
      invariant n >= i;
      invariant 0 >= 0;
      invariant contains(min, a, i);
      invariant lower_bound(n, a, 0);
      invariant lower_bound(min, a, i);
      invariant lower_bound(min, a, 0);
    {
        if (a[i] < min)
        {
            min := a[i];
        }

        i := i + 1;
    }

    return;
}



function lower_bound(v: int, a: [int]int, n: int) : bool;

axiom (forall v: int, a: [int]int, n: int :: { lower_bound(v, a, n): bool } lower_bound(v, a, n): bool <==> (forall j: int :: 0 <= j && j < n ==> a[j] >= v));

function is_min(m: int, a: [int]int, n: int) : bool;

axiom (forall m: int, a: [int]int, n: int :: { is_min(m, a, n): bool } is_min(m, a, n): bool <==> contains(m, a, n) && lower_bound(m, a, n));

procedure min_pred_v2(a: [int]int, n: int) returns (min: int);
  requires n > 0;
  ensures is_min(min, a, n);



implementation min_pred_v2(a: [int]int, n: int) returns (min: int)
{
  var i: int;

    min := a[0];
    i := 1;
    while (i < n)
      invariant n >= i;
      invariant n >= 0;
      invariant is_min(min, a, i);
    {
        if (a[i] < min)
        {
            min := a[i];
        }

        i := i + 1;
    }

    return;
}



procedure square_orig(n: int) returns (r: int);
  requires 0 <= n;
  ensures r == n * n;



implementation square_orig(n: int) returns (r: int)
{
  var x: int;
  var i: int;

    r := 0;
    i := 0;
    x := 1;
    while (i < n)
      invariant x > i;
      invariant x > 0;
      invariant r >= 0;
      invariant 0 >= 0;
    {
        r := r + x;
        x := x + 2;
        i := i + 1;
    }
}



procedure square_easy_v1(n: int) returns (r: int);
  requires 0 <= n;
  ensures r == n * n;



implementation square_easy_v1(n: int) returns (r: int)
{
  var i: int;

    i := 0;
    r := 0;
    while (i < n)
      invariant n >= i;
      invariant n >= 0;
      invariant r >= i;
      invariant r >= 0;
      invariant i >= 0;
      invariant 0 >= 0;
    {
        r := r + n;
        i := i + 1;
    }
}



procedure square_easy_v2(n: int, x: int) returns (r: int);
  requires 0 <= n;
  requires x == n;
  ensures r == x * n;



implementation square_easy_v2(n: int, x: int) returns (r: int)
{
  var i: int;

    i := 0;
    r := 0;
    while (i < n)
      invariant i <= x;
      invariant i <= r;
      invariant i >= 0;
      invariant 0 == 0;
      invariant r == i * x;
    {
        r := r + n;
        i := i + 1;
    }
}


