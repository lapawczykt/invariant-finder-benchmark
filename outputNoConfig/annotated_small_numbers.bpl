// Boogie program verifier version 2.3.0.61016, Copyright (c) 2003-2014, Microsoft.
// Command Line Options: 

procedure add_small_numbers(a: [int]int, n: int, max: int) returns (r: int);
  requires n > 0;
  requires (forall i: int :: 0 <= i && i < n ==> a[i] <= max);
  ensures r <= max * n;



implementation add_small_numbers(a: [int]int, n: int, max: int) returns (r: int)
{
  var i: int;

    i := 0;
    r := 0;
    while (i < n)
      invariant {:tested} true ==> true;
      invariant {:tested} false ==> true;
      invariant {:tested} false ==> false;
      invariant {:tested} 0 < n;
      invariant {:tested} n > 0;
      invariant {:tested} n <= n;
      invariant {:tested} max <= max;
      invariant {:tested} r <= r;
      invariant {:tested} i <= n;
      invariant {:tested} i <= i;
      invariant {:tested} 0 <= n;
      invariant {:tested} 0 <= i;
      invariant {:tested} 0 <= 0;
      invariant {:tested} n >= n;
      invariant {:tested} n >= i;
      invariant {:tested} n >= 0;
      invariant {:tested} max >= max;
      invariant {:tested} r >= r;
      invariant {:tested} i >= i;
      invariant {:tested} i >= 0;
      invariant {:tested} 0 >= 0;
      invariant {:tested} a == a;
      invariant {:tested} n == n;
      invariant {:tested} max == max;
      invariant {:tested} r == r;
      invariant {:tested} i == i;
      invariant {:tested} 0 == 0;
      invariant {:tested} true == true;
      invariant {:tested} false == false;
      invariant {:tested} n <= n * n;
      invariant {:tested} max <= max * max;
      invariant {:tested} r <= max * max;
      invariant {:tested} r <= max * i;
      invariant {:tested} r <= r * r;
      invariant {:tested} r <= r * i;
      invariant {:tested} r <= r * 0;
      invariant {:tested} r <= i * n;
    {
        r := r + a[i];
        i := i + 1;
    }
}


