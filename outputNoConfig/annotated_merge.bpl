// Boogie program verifier version 2.3.0.61016, Copyright (c) 2003-2014, Microsoft.
// Command Line Options: /proc:merge

function sorted(a: [int]int, fromIndex: int, toIndex: int) : bool;

axiom (forall a: [int]int, fromIndex: int, toIndex: int :: { sorted(a, fromIndex, toIndex): bool } sorted(a, fromIndex, toIndex): bool <==> (forall i: int, j: int :: fromIndex <= i && i <= j && j < toIndex ==> a[i] <= a[j]));

procedure merge(a: [int]int, start: int, mid: int, n: int) returns (b: [int]int);
  requires 0 <= start;
  requires start <= mid;
  requires mid <= n;
  requires sorted(a, start, mid);
  requires sorted(a, mid, n);
  ensures sorted(b, start, n);



implementation merge(a: [int]int, start: int, mid: int, n: int) returns (b: [int]int)
{
  var i: int;
  var l: int;
  var r: int;

    i, l, r := start, start, mid;
    while (i < n)
      invariant n >= i;
      invariant n >= l;
      invariant n >= r;
      invariant n >= 0;
      invariant i >= l;
      invariant i >= 0;
      invariant l >= start;
      invariant l >= l;
      invariant r >= mid;
      invariant r >= l;
    {
        if (r >= n || (l < mid && a[l] < a[r]))
        {
            b[i] := a[l];
            l := l + 1;
        }
        else
        {
            b[i] := a[r];
            r := r + 1;
        }

        i := i + 1;
    }
}


