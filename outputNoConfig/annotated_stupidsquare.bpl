// Boogie program verifier version 2.3.0.61016, Copyright (c) 2003-2014, Microsoft.
// Command Line Options: /proc:square

procedure square(n: int, x: int) returns (r: int);
  requires 0 <= n;
  requires x == n;
  ensures r == x * n;



implementation square(n: int, x: int) returns (r: int)
{
  var i: int;

    i := 0;
    r := 0;
    while (i < n)
      invariant x >= i;
      invariant r >= r;
      invariant r >= i;
      invariant i >= i;
      invariant r == i * x;
    {
        r := r + n;
        i := i + 1;
    }
}


