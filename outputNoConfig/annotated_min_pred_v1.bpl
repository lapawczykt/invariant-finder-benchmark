// Boogie program verifier version 2.3.0.61016, Copyright (c) 2003-2014, Microsoft.
// Command Line Options: /proc:min

function contains(v: int, a: [int]int, n: int) : bool;

axiom (forall v: int, a: [int]int, n: int :: { contains(v, a, n): bool } contains(v, a, n): bool <==> (exists j: int :: 0 <= j && j < n && a[j] == v));

function lower_bound(v: int, a: [int]int, n: int) : bool;

axiom (forall v: int, a: [int]int, n: int :: { lower_bound(v, a, n): bool } lower_bound(v, a, n): bool <==> (forall j: int :: 0 <= j && j < n ==> a[j] >= v));

procedure min(a: [int]int, n: int) returns (min: int);
  requires n > 0;
  ensures contains(min, a, n);
  ensures lower_bound(min, a, n);



implementation min(a: [int]int, n: int) returns (min: int)
{
  var i: int;

    min := a[0];
    i := 1;
    while (i < n)
      invariant n >= i;
      invariant n >= 0;
      invariant contains(min, a, i);
      invariant lower_bound(n, a, 0);
      invariant lower_bound(min, a, i);
      invariant lower_bound(min, a, 0);
    {
        if (a[i] < min)
        {
            min := a[i];
        }

        i := i + 1;
    }

    return;
}


