// Boogie program verifier version 2.3.0.61016, Copyright (c) 2003-2014, Microsoft.
// Command Line Options: /proc:max_pred_v2

procedure max_orig(a: [int]int, n: int) returns (max: int);
  requires n > 0;
  ensures (exists i: int :: 0 <= i && i < n && a[i] == max);
  ensures (forall i: int :: 0 <= i && i < n ==> a[i] <= max);



implementation max_orig(a: [int]int, n: int) returns (max: int)
{
  var i: int;

    max := a[0];
    i := 1;
    while (i < n)
      invariant n >= i;
      invariant n >= 0;
      invariant (exists boundVar0: int :: 0 <= boundVar0 && boundVar0 < i && a[boundVar0] == max);
      invariant (forall boundVar0: int :: n <= n && n < n ==> a[n] <= n);
      invariant (forall boundVar0: int :: 0 <= boundVar0 && boundVar0 < i ==> a[boundVar0] <= max);
      invariant (forall boundVar0: int :: 0 <= boundVar0 && boundVar0 < boundVar0 ==> a[boundVar0] <= n);
    {
        if (a[i] > max)
        {
            max := a[i];
        }

        i := i + 1;
    }

    return;
}



function contains(v: int, a: [int]int, n: int) : bool;

axiom (forall v: int, a: [int]int, n: int :: { contains(v, a, n): bool } contains(v, a, n): bool <==> (exists j: int :: 0 <= j && j < n && a[j] == v));

function upper_bound(v: int, a: [int]int, n: int) : bool;

axiom (forall v: int, a: [int]int, n: int :: { upper_bound(v, a, n): bool } upper_bound(v, a, n): bool <==> (forall j: int :: 0 <= j && j < n ==> a[j] <= v));

procedure max_pred_v1(a: [int]int, n: int) returns (max: int);
  requires n > 0;
  ensures contains(max, a, n);
  ensures upper_bound(max, a, n);



implementation max_pred_v1(a: [int]int, n: int) returns (max: int)
{
  var i: int;

    max := a[0];
    i := 1;
    while (i < n)
      invariant n >= i;
      invariant n >= 0;
      invariant i >= 0;
      invariant 0 >= 0;
      invariant contains(max, a, n);
      invariant upper_bound(max, a, i);
    {
        if (a[i] > max)
        {
            max := a[i];
        }

        i := i + 1;
    }

    return;
}



function is_max(m: int, a: [int]int, n: int) : bool;

axiom (forall m: int, a: [int]int, n: int :: { is_max(m, a, n): bool } is_max(m, a, n): bool <==> contains(m, a, n) && upper_bound(m, a, n));

procedure max_pred_v2(a: [int]int, n: int) returns (max: int);
  requires n > 0;
  ensures is_max(max, a, n);



implementation max_pred_v2(a: [int]int, n: int) returns (max: int)
{
  var i: int;

    max := a[0];
    i := 1;
    while (i < n)
      invariant i > 0;
      invariant max <= max;
      invariant n >= i;
      invariant n >= 0;
    {
        if (a[i] > max)
        {
            max := a[i];
        }

        i := i + 1;
    }

    return;
}


