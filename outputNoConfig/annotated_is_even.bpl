// Boogie program verifier version 2.3.0.61016, Copyright (c) 2003-2014, Microsoft.
// Command Line Options: /proc:is_even

function even(n: int) : bool;

axiom even(0);

axiom (forall i: int :: i > 0 ==> (even(i) <==> !even(i - 1)));

procedure is_even(n: int) returns (r: bool);
  requires n >= 0;
  ensures r <==> even(n);



implementation is_even(n: int) returns (r: bool)
{
  var i: int;

    i := 0;
    r := true;
    while (i < n)
      invariant n >= i;
      invariant n >= 0;
      invariant i >= 0;
      invariant 0 >= 0;
      invariant r <==> even(i);
      invariant true <==> even(0);
    {
        r := !r;
        i := i + 1;
    }
}


